import pandas as pd
import seaborn as sns; sns.set()
import matplotlib.pyplot as plt
import numpy as np
import plotly.offline as py
import plotly.graph_objs as go

import matplotlib
matplotlib.rcParams['figure.figsize'] = (16,8)

#consulta aos arquivos do BC
def consulta_bc(codigo_bcb):
  url = 'http://api.bcb.gov.br/dados/serie/bcdata.sgs.{}/dados?formato=json'.format(codigo_bcb)
  df = pd.read_json(url)
  df['data'] = pd.to_datetime(df['data'], dayfirst=True)
  df.set_index('data', inplace=True)
  return df

#anualizar os valores
def anualiza_serie(ipca_gen):
    ipca_gen = (ipca_gen/100 + 1 )
    ipca_gen = np.power((ipca_gen),12)
    ipca_gen = (ipca_gen -1 ) * 100
    return ipca_gen

#corte a partir da data inicial
def corta_data(data, ipca_gen):
    ipca_gen = ipca_gen[ipca_gen.index >= data]
    return ipca_gen

#gerar e salvar os gráficos
def grafico(nome, ipca_gen):
    ipca_gen.plot(xlabel = "Data", ylabel = "Variação % mensal anualizada", title = nome)
    plt.savefig(nome + ".png")
    plt.clf()

#capturando os dados
ipca = consulta_bc(433)
ipca_ex0 = consulta_bc(11427) #ex0
ipca_ex1 = consulta_bc(16121) #ex1 Núcleo por exclusão -
ipca_ex2 = consulta_bc(27838) #ex2
ipca_ex3 = consulta_bc(27839) #ex3
ipca_ma = consulta_bc(11426) # ma Núcleo médias aparadas sem suavização
ipca_ms = consulta_bc(4466) # ms Núcleo médias aparadas com suavização
ipca_dp = consulta_bc(16122) #dp

#data que quero começar
data_inicio = '2006-01-01'

#realizo o corte dos dados a partir da data_inicial
ipca_d = corta_data(data_inicio, ipca)
ipca_ms_d = corta_data(data_inicio, ipca_ms)
ipca_ma_d = corta_data(data_inicio, ipca_ma)
ipca_ex0_d = corta_data(data_inicio, ipca_ex0)
ipca_ex1_d = corta_data(data_inicio, ipca_ex1)
ipca_dp_d = corta_data(data_inicio, ipca_dp)
ipca_ex2_d = corta_data(data_inicio, ipca_ex2)
ipca_ex3_d = corta_data(data_inicio, ipca_ex3)

#anualização dos dados
ipca_d = anualiza_serie(ipca_d)
ipca_ms_d = anualiza_serie(ipca_ms_d)
ipca_ma_d = anualiza_serie(ipca_ma_d)
ipca_ex0_d = anualiza_serie(ipca_ex0_d)
ipca_ex1_d = anualiza_serie(ipca_ex1_d)
ipca_dp_d = anualiza_serie(ipca_dp_d)
ipca_ex2_d = anualiza_serie(ipca_ex2_d)
ipca_ex3_d = anualiza_serie(ipca_ex3_d)

#gerar e salvar gráficos
grafico("IPCA",ipca_d)
grafico("IPCA-Núcleo médias aparadas com suavização-MS",ipca_ms_d)
grafico("IPCA-Núcleo médias aparadas sem suavização-MA",ipca_ma_d)
grafico("IPCA-Núcleo por exclusão-EX0",ipca_ex0_d)
grafico("IPCA-Núcleo por exclusão-EX1",ipca_ex1_d)
grafico("IPCA-Núcleo de dupla ponderação-DP",ipca_dp_d)
grafico("IPCA-Núcleo por exclusão-EX2",ipca_ex2_d)
grafico("IPCA-Núcleo por exclusão-EX3",ipca_ex3_d)
